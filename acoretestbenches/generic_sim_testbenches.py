"""
generic_sim_testbenches
=======================

Python module containing simulation testbench classes for A-Core.


"""
import os
import sys
import glob
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *
from acorechip import ACoreChip
from acorechip.controller import jtag_controller
from acorechip.controller import controller as ACoreChip_controller
from jtag.driver import jtag_driver
from jtag_programmer import jtag_programmer
from acorechip.acore_compilers import ACoreChiselCompiler, ACoreSWCompiler
from abc import ABC
import numpy as np
import cocotb_test.simulator
import acoretestbenches.common as common


class GenericTestbench(thesdk, ABC):
    """
    Abstract class for testbenches. Extend your custom testbench with this class or its child class.

    Example
    -------
    .. code-block:: python

        class CustomTestbench(GenericTestbench)


    Parameters
    ----------
    test_config : dict
        Test configuration
    test_name : str
        Optional name for the test
    cache_dir : str
        Path to the cache directory. By default it is under ``.cache``
    """
    def __init__(self, **kwargs):
        self.test_config      = kwargs.get("test_config", None)
        self.test_name        = kwargs.get("test_name", "test")

        self.cache_dir             = kwargs.get("cache_dir", os.path.join(self.entitypath, ".cache"))

        # Properties initialized by init functions
        self.chisel_cache_dir      = None
        self.test_cache_dir        = None
        self.hw_config             = None
        self.jtag_config           = None
        self.sim_config            = None
        self.acore_lib_path        = None
        self.chisel_compiler       = None
        self.sw_compiler           = None
        self.dut                   = None
        self.controller            = None
        self.jtag_driver           = None
        self.jtag_programmer       = None
        self.jtag_ctrl             = None
        self.sim_timeout           = None
        self.skip_chisel_clean     = None
        self.skip_chisel_configure = None
        self.skip_chisel_compile   = None
        self.skip_sw_compile       = None
        self.platform              = None
        self.use_cached_chisel     = None
        self.verilog_sources       = None
        self.vhdl_sources          = None

        super().__init__()

    def init(self):
        """
        Default generic init function.
        Use this in your custom testbench by calling ``super().init()``.
        """
        self.load_configs()
        self.init_properties()
        self.init_cache_locations()
        self.init_dut()
        self.init_jtag()
        self.init_compilers()

    def run(self):
        """
        Default generic run function.
        Runs chisel and sw compilers and creates symbolic links to
        rtl simulation folder under ``Entities/acorechip/simulations``.
        Use this in your custom testbench by calling ``super().run()``.
        """
        self.chisel_compiler.run()
        self.sw_compiler.run()
        self.symlink_sources()

    def load_configs(self):
        """
        Load configuration files from ``self.test_config``.
        """
        self.hw_config   = self.test_config["hw_config"]
        self.jtag_config = self.hw_config["jtag_config"]
        self.sim_config = self.test_config["sim_config"]

    def init_cache_locations(self):
        """
        Defines the cache location for cached chisel products. 
        """
        self.chisel_cache_dir = os.path.join(self.cache_dir, 'chisel', self.hw_config["isa_string"])
        self.test_cache_dir = os.path.join(self.cache_dir, 'tests', self.test_name)
        try:
            os.makedirs(self.test_cache_dir)
        except FileExistsError:
            pass

    def init_compilers(self):
        """
        Initialize compilers.
        """
        self.skip_chisel_clean = False
        self.skip_chisel_configure = False
        self.skip_chisel_compile = False
        if self.use_cached_chisel:
            if os.path.isfile(os.path.join(self.chisel_cache_dir, "acorechip.v")):
                self.skip_chisel_clean = True
                self.skip_chisel_configure = True
                self.skip_chisel_compile = True
                self.print_log(type='I', msg='Using cached version for chisel.')
        if self.skip_sw_compile:
                self.print_log(type='I', msg='Using previously compiled elf.')
        self.chisel_compiler = ACoreChiselCompiler(
            hw_config_path=self.test_config["hw_config_yaml"],
            jtag_config_path=self.hw_config["jtag_config_yaml"],
            chisel_dir=os.path.join(self.dut.entitypath, "chisel"),
            target_dir=self.chisel_cache_dir,
            gen_cheaders=True,
            cheaders_target_dir=os.path.join(self.chisel_cache_dir, "include"),
            make_args=[],
            skip_clean=self.skip_chisel_clean,
            skip_configure=self.skip_chisel_configure,
            skip_compile=self.skip_chisel_compile
        )
        self.sw_compiler = ACoreSWCompiler(
            test_config=self.test_config,
            hw_config=self.hw_config,
            acore_lib_path=self.acore_lib_path,
            acore_headers_path=os.path.join(self.chisel_cache_dir, "include"),
            platform=self.platform,
            skip_clean=self.skip_sw_compile,
            skip_compile=self.skip_sw_compile,
            target_dir=self.test_cache_dir
        )
        self.chisel_compiler.init()
        self.sw_compiler.init()

    def init_dut(self):
        """
        Initialize ACoreChip TheSydekick module. 
        """
        self.dut = ACoreChip()
        self.dut.vlogext = '.v'
        self.dut.init()
        self.dut.Rs = self.sim_config.get("clock_freq", 1e9)
        self.dut.rtl_timescale = self.sim_config.get("rtl_timescale", "1ps")
        self.dut.rtl_timeprecision = self.sim_config.get("rtl_timeprecision", "1ps")
        self.dut.add_tb_timescale = True


    def init_jtag(self):
        """
        Initialize JTAG.
        """
        self.jtag_driver = jtag_driver()
        namemap = {
            'address':  'prog_iface_addr',
            'data':     'prog_iface_data',
            'write_en': 'prog_iface_write_en'
        }
        self.jtag_programmer = jtag_programmer(
            driver=self.jtag_driver,
            config=self.jtag_config,
            namemap=namemap
        )
        self.jtag_ctrl = jtag_controller(self.jtag_driver, self.jtag_config)

    def init_properties(self):
        """
        Initialize generic properties. 
        """
        self.platform              = self.sim_config.get("platform", "sim")
        self.use_cached_chisel     = self.sim_config.get("use_cached_chisel", False)
        self.skip_sw_compile       = self.sim_config.get("skip_sw_compile", False)

    def symlink_sources(self):
        """
        Symlink source RTL files from chisel cache dir and 
        ``extra_source``s (defined in hw config) to RTL simulation folder.
        RTL simulation folder is typically located under ``Entities/acorechip/simulations``.
        """

        # Take sources from chisel cache dir
        cache_verilog_sources = glob.glob(os.path.join(self.chisel_cache_dir, '*.v')) \
            + glob.glob(os.path.join(self.chisel_cache_dir, '*.sv'))
        cache_vhdl_sources = glob.glob(os.path.join(self.chisel_cache_dir, '*.vhd')) \
            + glob.glob(os.path.join(self.chisel_cache_dir, '*.vhdl'))

        extra_verilog_sources = []
        extra_vhdl_sources = []
        # Take extra sources from hw config
        for extra_source in self.hw_config.get("extra_sources", []):
            if extra_source.endswith(".v") or extra_source.endswith(".sv"):
                extra_verilog_sources += [extra_source]
            elif extra_source.endswith(".vhd") or extra_source.endswith(".vhdl"):
                extra_vhdl_sources += [extra_source]
            else:
                self.print_log(type='W', msg=f'Unrecognized file for hardware source:  {extra_source}')
            self.dut.rtlfiles += [os.path.basename(extra_source)]

        self.dut.rtlfiles += [os.path.basename(src) for src in (cache_vhdl_sources + cache_verilog_sources)]
        self.verilog_sources = cache_verilog_sources + extra_verilog_sources
        self.vhdl_sources = cache_vhdl_sources + extra_vhdl_sources

        for src in (self.verilog_sources + self.vhdl_sources):
            dstfile = os.path.join(self.dut.rtlsimpath, os.path.basename(src))
            os.symlink(src, dstfile)

class GenericTheSydekickSimTestbench(GenericTestbench):
    """
    Testbench class for running simulations using TheSydeKick framework.

    Example
    -------
    .. code-block:: python

        sdk_tb = GenericTheSydekickSimTestbench()
        sdk_tb.init()
        sdk_tb.run()

    See ``sim_configs/thesdk.yml`` for explanations on different parameters.

    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.print_log(type='I', msg='Inititalizing %s' %(__name__))

    def init_simulator(self):
        """
        Initialize simulator.
        """
        self.sim_timeout = self.sim_config.get("timeout", 1000000)
        self.dut.preserve_rtlfiles = self.sim_config.get("preserve_rtlfiles", False)
        self.dut.preserve_iofiles = self.sim_config.get("preserve_iofiles", False)
        self.dut.interactive_rtl = self.sim_config.get("interactive_sim", False)
        self.backend = self.sim_config.get("backend", "icarus")
        self.selfchecking = self.sim_config.get("selfchecking", False)
        self.dut.interactive_controlfile = self.sim_config.get(
            "interactive_controlfile",
            [os.path.join(self.dut.entitypath, "interactive_control_files/modelsim/dofile.do") if self.backend == "questa" \
             else os.path.join(self.dut.entitypath, "interactive_control_files/gtkwave/general.tcl")]
        )
        if self.backend == "questa":
            self.dut.model = 'sv'
        elif self.backend == "icarus":
            self.dut.model = 'icarus'
        if self.selfchecking:
            self.dut.add_simstop()

    def define_sim_flow(self):
        """
        Define simulation flow.
        """
        self.controller.reset()
        self.controller.step_time()
        self.controller.start_datafeed()
        self.controller.step_time(step=len(self.dut.IOS.Members['jtag_tap_in'].Data)*self.controller.step)
        self.controller.set_progdone()
        self.controller.step_time(step=self.sim_timeout*self.controller.step)
        self.controller.set_simdone()

    def generate_jtag_stream(self):
        """
        Generate JTAG stream. Store it in ``jtag_tap_in`` IO.
        """
        self.jtag_driver.clear()
        self.jtag_driver.reset_tap()
        self.jtag_ctrl.set_core_en(0)
        self.jtag_programmer.read_elf(self.sw_compiler.bin)
        self.jtag_programmer.run()
        self.jtag_ctrl.set_core_en(1)
        self.dut.IOS.Members['jtag_tap_in'].Data = self.jtag_driver.get_numpy_array()

    def init_dut_controller(self):
        """
        Initialize ACoreChip controller.
        """
        self.controller = ACoreChip_controller(dut=self.dut)
        self.dut.IOS.Members['control_write'] = self.controller.IOS.Members['control_write']

    def check_sim_result(self):
        self.result = 0
        try:
            self.result = int(self.dut.IOS.Members['simresult'].Data[0][0])
        except TypeError: # Raised if there is no data in simresult
            self.result = 0

    def init(self, **kwargs):
        super().init()
        self.init_simulator()

    def run(self):
        super().run()
        self.init_dut_controller()
        self.generate_jtag_stream()
        self.define_sim_flow()
        self.dut.run()
        if self.selfchecking: 
            self.check_sim_result()
            if self.result != 1:
                raise common.TestFailError

class GenericCocotbSimTestbench(GenericTestbench):
    """
    Testbench class for running simulations using cocotb framework.

    Example
    -------
    .. code-block:: python

        cocotb_tb = GenericCocotbSimTestbench()
        cocotb_tb.init()
        cocotb_tb.run()

    See ``sim_configs/cocotb.yml`` for explanations on different parameters.

    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.print_log(type='I', msg='Inititalizing %s' %(__name__))

    def init_simulator(self):
        """
        Initialize simulator.
        """
        self.test_module = self.sim_config.get("test_module", "acoretestbenches.test_cocotb")
        self.sim_timeout = self.sim_config.get("timeout", 1000000)
        self.backend = self.sim_config.get("backend", "icarus")
        self.dut.Rs = self.sim_config.get("clock_freq", 1e9)
        self.wave_fmt = self.sim_config.get("wave_fmt", "vcd")
        self.interactive_sim = self.sim_config.get("interactive_sim", False)
        self.waves = self.sim_config.get("interactive_sim", False)
        self.selfchecking = self.sim_config.get("selfchecking", False)
        self.spike_cmd = self.sim_config.get("spike_cmd", "/prog/riscv/bin/spike")
        self.sim_args = []
        self.vlog_compile_args = []
        self.vhdl_compile_args = []
        self.dump_location = os.getcwd()
        self.dump_target = f"acorechip.{self.wave_fmt}"
        if self.backend == "questa":
            # Questa modifies LD_LIBRARY_PATH internally if not set
            # This makes it use a very old gcc that is not compatible with
            # modern cocotb or numpy
            self.sim_args += ['-noautoldlibpath']
            self.vhdl_compile_args += ['-2008']
            if self.wave_fmt not in ["vcd"]:
                self.print_log(type='F', msg=f"Invalid wave format: {self.wave_fmt}. Supported: vcd.")
        elif self.backend == "icarus":
            if self.wave_fmt not in ["vcd", "fst"]:
                self.print_log(type='F', msg=f"Invalid wave format: {self.wave_fmt}. Supported: fst, vcd.")
            elif self.wave_fmt == 'vcd':
                self.waves = False
        elif self.backend == "verilator":
            # Waive warnings
            self.vlog_compile_args += ['-Wno-WIDTHEXPAND'] + ['-Wno-ASCRANGE']
            self.dump_target = f"dump.{self.wave_fmt}"
            if self.wave_fmt == "vcd":
                if self.waves:
                    self.vlog_compile_args += ["--trace"]
                self.waves = False
        else:
            self.print_log(type='F', msg=f"Invalid backend for cocotb testbench: {self.backend}. Supported: questa, icarus, verilator")
    
    def cocotbify(self):
        """
        "cocotbify" the verilog - allows generating VCD trace of the simulation.
        Needed for icarus runs.
        """
        cocotbify_cmd = 'cocotbify -v ' + self.dut.simdut + ' -o ' + self.dut.simdut
        self.print_log(type='I', msg="Running command: %s" % cocotbify_cmd)
        try:
            os.system(cocotbify_cmd)
        except Exception as e:
            self.print_log(type='I', msg="Already cocotbifyied.")

    def generate_jtag_stream(self):
        """
        Generate JTAG stream and save it in a tempfile.
        """
        self.jtag_driver.clear()
        self.jtag_driver.reset_tap()
        self.jtag_ctrl.set_core_en(0)
        self.jtag_ctrl.set_core_en(1)
        self.jtag_file = open(os.path.join(self.test_cache_dir, "jtag.txt"), "w")
        np.savetxt(self.jtag_file.name, self.jtag_driver.get_numpy_array())
        self.jtag_file.close()

    def compose_args(self):
        """
        Compose simulation and compilation arguments for cocotb.
        """
        os.environ["SIM"] = self.backend
        self.run_args = {
            "verilog_sources": self.verilog_sources,
            "vhdl_sources": self.vhdl_sources,
            "work_dir": os.getcwd(),
            "toplevel": "acorechip",
            "module": self.test_module,
            "verilog_compile_args": self.vlog_compile_args,
            "vhdl_compile_args": self.vhdl_compile_args,
            "sim_args": self.sim_args,
            "waves": self.waves,
            "timescale": f"{self.dut.rtl_timescale}/{self.dut.rtl_timeprecision}",
            "plus_args":
                ["+jtag_file=" + self.jtag_file.name,
                 "+elf=" + self.sw_compiler.bin,
                 "+selfchecking=" +
                 str(self.selfchecking),
                 "+pc_init=" + str(self.hw_config['pc_init']),
                 "+progmem_depth=" +
                 str(self.hw_config['progmem_depth']),
                 "+datamem_depth=" +
                 str(self.hw_config['datamem_depth']),
                 "+isa_string=" +
                 str(self.hw_config['isa_string']),
                 "+timeout=" + str(self.sim_timeout),
                 "+clock_freq=" + str(self.dut.Rs),
                 "+timescale=" + str(self.dut.rtl_timescale_num),
                 "+spike_cmd=" + str(self.spike_cmd)]
        }

    def run_cocotb(self):
        """
        Run cocotb testbench.
        """
        if self.backend == "questa":
            cocotb_runner = CustomQuestaRunner(**self.run_args)
            cocotb_runner.run()
        else:
            cocotb_test.simulator.run(**self.run_args)

    def init(self):
        super().init()
        self.init_simulator()

    def postsim_logs(self):
        """
        Prints a command for opening waveform file after simulation ends.
        """
        self.dump_path = os.path.join(self.test_cache_dir, self.dump_target)
        self.print_relative_path = False
        if self.interactive_sim:
            os.replace(os.path.join(self.dump_location, self.dump_target), self.dump_path)
            self.print_log(type='I', msg=(
                f"Waves generated to {self.dump_path}.\n"
                f"Open with:\ngtkwave --tcl_init="
                f"{os.path.join(self.dut.entitypath, 'interactive_control_files/gtkwave/general.tcl')} {self.dump_path}"
            ))
        self.print_log(type='I', msg=(
            f"If you were using spike, see sim log at:\n"
            f"{os.path.join(self.dut.HOME, 'Entities/acorestests/acoretests/sim.log')}"
        ))

    def run(self):
        super().run()
        self.generate_jtag_stream()
        self.compose_args()
        if self.backend == 'icarus' and self.wave_fmt == "vcd" and self.interactive_sim:
            self.cocotbify()
        try:
            self.run_cocotb()
            self.result = 1
            self.postsim_logs()
        except BaseException as e:
            self.print_log(type='E', msg=str(e))
            self.result = 0
            self.postsim_logs()
            raise common.TestFailError


class CustomQuestaRunner(cocotb_test.simulator.Questa):
    """Custom runner for cocotb+questa combo. Overrides do_script
    generation, because the default one does not work for our setup for
    generating wave dumps.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def do_script(self):
        do_script = ""
        if self.waves:
            do_script += "vcd file acorechip.vcd;vcd add -r *;"
        if not self.gui:
            do_script += "run -all; quit"
        return do_script








