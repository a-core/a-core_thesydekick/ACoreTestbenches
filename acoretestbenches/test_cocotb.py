# SPDX-License-Identifier: Apache-2.0

# pip install cocotb
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import ClockCycles, Edge, Timer, with_timeout
import numpy as np
import acoretestbenches.common as common
@cocotb.test()
async def test_acorechip(dut):
    """
    This test loads the program from an elf file into ROM, toggles reset, and runs the clock
    for a specified number of clock cycles. 
    
    A-Core's mstopsim CSR is monitored - once its 0th bit
    is set to 1, simulation is stopped. Simulation result is stored in its 1st bit. Otherwise,
    a timeout value is used. If the test is set as selfchecking (i.e. it writes to mstopsim),
    and timeout is reached, cocotb throws a SystemError. An AssertionError is thrown if
    mstopsim 1st bit is 0 after simulation is stopped.

    This testbench uses JTAG to set A-Core's core_en because, with verilator, it is not possible to
    write to internal signals (except memory, seemingly).
    """
    
    async def jtag_cycle(tck, tms, tdi, trstn):
        """
        Execute one jtag cycle.
        """
        try:
            dut.io_jtag_TMS.value = tms
            dut.io_jtag_TDI.value = tdi
            dut.io_jtag_TCK.value = tck
            dut.io_jtag_TRSTn.value = trstn
            await ClockCycles(clock, 1)
        except Exception as e:
            print(e)

    elf_path = cocotb.plusargs["elf"]
    selfchecking = cocotb.plusargs["selfchecking"] == "True"
    jtag_file = cocotb.plusargs["jtag_file"]
    timescale = float(cocotb.plusargs["timescale"])
    clock_freq = int(cocotb.plusargs["clock_freq"])
    clock_step = int(round(1 / (clock_freq * timescale)))
    # timeout is provided in clock cycles
    # this converts it to steps
    timeout = int(cocotb.plusargs["timeout"]) * clock_step

    if   timescale >= 1e-3:  unit = "ms"
    elif timescale >= 1e-6:  unit = "us"
    elif timescale >= 1e-9:  unit = "ns"
    elif timescale >= 1e-12: unit = "ps"
    elif timescale >= 1e-15: unit = "fs"
    else: raise ValueError(f"too small timescale: {timescale}")

    if not elf_path:
        raise Exception("Please provide an elf file.")
    elf_file = common.read_elf(elf_path)

    clock = dut.clock
    reset = dut.reset

    # Dummy values to input IOs
    dut.io_gpi.value = 0x55

    # Clock process
    cocotb.start_soon(Clock(clock, clock_step, units=unit).start())

    await jtag_cycle(0, 0, 0, 0)

    # Load JTAG signals from input file
    jtag_array = np.loadtxt(jtag_file)
    
    # Initialize ROM from elf file
    rom_blob = common.rom_image(elf_file)
    rom_blocks = [
        dut.progmem.mem_array_0,
        dut.progmem.mem_array_1,
        dut.progmem.mem_array_2,
        dut.progmem.mem_array_3
        ]
    for i in range(len(rom_blob)):
        rom_blocks[i%len(rom_blocks)][i//len(rom_blocks)].value = int(rom_blob[i])

    # Reset DUT
    reset.value = 0
    await ClockCycles(clock, 2)
    reset.value = 1
    await ClockCycles(clock, 4)
    reset.value = 0
    await ClockCycles(clock, 2)

    # Run JTAG (e.g. asserting core_en)
    for item in jtag_array:
        await jtag_cycle(int(item[0]), int(item[1]), int(item[2]), int(item[3]))

    if selfchecking:
        # Run until the sw program writes to mstopsim
        await with_timeout(Edge(dut.core.csreg_block.csregs.mstopsim),
                           timeout_time=timeout, timeout_unit="step")
        assert dut.core.csreg_block.csregs.mstopsim.value == 3
    else:
        # Run for a speficied number of clock cycles
        await Timer(timeout, units="step")
