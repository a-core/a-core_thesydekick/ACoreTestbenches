# SPDX-License-Identifier: Apache-2.0

# pip install cocotb
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import ClockCycles, RisingEdge
import numpy as np
import acoretestbenches.common as common
import logging

TIMEOUT = 100000000000

# NOTE: icarus supports only 'step's as time unit, therefore we should use only them


# Timeout is 2x'd because a clock cycle is 2 steps
@cocotb.test(timeout_time=2 * TIMEOUT, timeout_unit="step")
async def test_acorechip(dut):
    """
    This test is similar to :any:`acoretestbenches.test_cocotb.test_acorechip`,
    but it runs the `Spike <https://github.com/riscv-software-src/riscv-isa-sim>`_ 
    simulator in parallel. Every time an instruction is retired, the program counter and register values are compared
    between A-Core and Spike. If there is a mismatch, simulation is halted.

    The test generates a logfile ``sim.log``. It is generated to the location where
    this module was called, so most likely ``Entities/acoretests/acoretests``.
    """

    import py_spike.config
    import py_spike.runner

    async def jtag_cycle(tck, tms, tdi, trstn):
        """
        Execute one jtag cycle.
        """
        try:
            dut.io_jtag_TMS.value = tms
            dut.io_jtag_TDI.value = tdi
            dut.io_jtag_TCK.value = tck
            dut.io_jtag_TRSTn.value = trstn
            await ClockCycles(clock, 1)
        except Exception as e:
            print(e)

    elf_path = cocotb.plusargs["elf"]
    pc_init = cocotb.plusargs["pc_init"].replace("h", "0x")
    progmem_depth = int(cocotb.plusargs["progmem_depth"])
    datamem_depth = int(cocotb.plusargs["datamem_depth"])
    isa_string = str(cocotb.plusargs["isa_string"])
    jtag_file = cocotb.plusargs["jtag_file"]
    spike_cmd = cocotb.plusargs["spike_cmd"]
    if not elf_path:
        raise Exception(
            "Please provide an elf file. Example: make ELF_FILE=/path/to/elf.elf"
        )
    elf_file = common.read_elf(elf_path)

    clock = dut.clock
    reset = dut.reset

    # Dummy values to input IOs
    dut.io_gpi.value = 0x55

    # Clock is 2 steps per cycle because icarus does not support 0.5 cycles
    cocotb.start_soon(Clock(clock, 2, units="step").start())
    await jtag_cycle(0, 0, 0, 0)

    jtag_array = np.loadtxt(jtag_file)

    # Initialize ROM
    rom_blob = common.rom_image(elf_file)
    rom_blocks = [
        dut.progmem.mem_array_0,
        dut.progmem.mem_array_1,
        dut.progmem.mem_array_2,
        dut.progmem.mem_array_3,
    ]
    for i in range(len(rom_blob)):
        rom_blocks[i % len(rom_blocks)][i // len(rom_blocks)].value = int(rom_blob[i])

    reset.value = 0

    await ClockCycles(clock, 2)
    reset.value = 1
    await ClockCycles(clock, 2)
    reset.value = 0
    await ClockCycles(clock, 2)

    for item in jtag_array:
        await jtag_cycle(int(item[0]), int(item[1]), int(item[2]), int(item[3]))
        if dut.core.io_core_en.value == 1:
            break

    test_spike = py_spike.config.SpikeConfig(
        elf_path,
        debug=True,
        spike_cmd=spike_cmd,
        pc=pc_init,
        disable_dtb=True,
        isa=isa_string.upper(),
        timeout=2,
    )
    rom = py_spike.config.MemoryRegion(pc_init, 2**progmem_depth)
    ram = py_spike.config.MemoryRegion("0x20000000", 2**datamem_depth)
    test_spike.add_memory_region(rom)
    test_spike.add_memory_region(ram)
    spike_runner = py_spike.runner.SpikeRunner(test_spike)
    spike_runner.start_sim()

    logging.basicConfig(
        filename="sim.log",
        filemode="w",
        format="%(message)s",
        level=logging.DEBUG,
        force=True,
    )

    while True:  # Runs until mstopsim or timeout
        await RisingEdge(clock)
        if dut.core.csreg_block.csregs.mstopsim == 1:
            break
        if dut.core.csreg_block_io_writeIn_control_instret_en.value == 1:
            spike_pc = int(spike_runner.get_pc(0), 16)
            acore_pc = int(dut.core.memd_wb_reg_pc.value)
            logging.debug(f"PC:  S:{hex(spike_pc):<12} A:{hex(acore_pc):<12}")
            assert spike_pc == acore_pc
            spike_regs = spike_runner.get_regs(0, use_dict=False)
            for i in range(0, 32):
                spike_reg = int(spike_regs[i], 16)
                acore_reg = int(getattr(dut.core.regfile_block.regfile, f"x_{i}").value)
                if spike_reg != acore_reg:
                    logging.debug(
                        f"x{i:>2}: S:{hex(spike_reg):<12} A:{hex(acore_reg):<12}"
                    )
                    logging.debug(
                        "Mismatch found! Press q and Enter to quit, "
                        "anything else to continue."
                    )
                    ui = input()
                    if ui == "q":
                        assert spike_reg == acore_reg
                    else:
                        continue
            spike_runner.step()
