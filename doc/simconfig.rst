Simulation config
=================

Simulation parameters are handled with simulation configuration YAML files. The config is automatically read by :class:`ACoreTestConfig<acorechip.config.ACoreTestConfig>` and turned into a dictionary, accessible from the testbench in ``self.sim_config``.

Example sim config: `thesdk.yml`

.. literalinclude:: thesdk.yml
  :language: YAML
