A-Core Testbenches
==================

This entity holds generic testbenches for running software on A-Core. The testbenches have been designed to be easy to extend, to allow creating custom testbenches for various applications.

.. toctree::
   :maxdepth: 2

   examples
   simconfig

API
---

.. toctree::
   :maxdepth: 2

   api

