Examples
========

Using only the testbench
------------------------

If the functionality of the generic testbenches are sufficient, you can simply import :class:`GenericTheSydekickSimTestbench<acoretestbenches.generic_sim_testbenches.GenericTheSydekickSimTestbench>` in your code, load a test config, and give it as a parameter.

.. code-block:: python

   from acorechip.config import *
   from acoretestbenches.generic_sim_testbenches import GenericTheSydekickSimTestbench
   ...
   test_config_path = "path/to/test-config.yml"
   config_loader = ACoreTestConfig(config_path = test_config_path)
   config_loader.run()
   acore_lib_path = os.path.join(thesdk.HOME, "Entities/acoresoftware/lib/a-core-library")
   tb = GenericTheSydekickSimTestbench(test_config=config_loader.config, test_name="test-name")
   tb.acore_lib_path = acore_lib_path
   tb.init()
   tb.run()

.. note:: 

   You cannot use :class:`GenericTests<acoretests.generic_tests.GenericTests>` command-line interface with this method.

Creating custom testbench
-------------------------
If you have a need to completely redefine the testbench flow, you can do so by defining your own testbench class. 

.. code-block:: python

   from acoretestbenches.generic_sim_testbenches import GenericTheSydekickSimTestbench
   ...
   class CustomTestbench(GenericTheSydekickSimTestbench):

Ensure your testbench can be initialized with ``self.init()`` and run with ``self.run()``.


Calling custom testbench with generic test interface
----------------------------------------------------
There is a built-in capability to run a custom testbench with :class:`GenericTests<acoretests.generic_tests.GenericTests>`. In `sim_config`, you can select ``tb_module`` and ``tb_class``. Set the former to be the same as the Python module, and the latter to be the testbench class. The module and class will be dynamically imported by :class:`GenericTests<acoretests.generic_tests.GenericTests>`.

:class:`GenericTests<acoretests.generic_tests.GenericTests>` assumes that the called testbench:

- has an attribute called `acore_lib_path`
- has an attribute called `test_name`
- has a method ``init()``
- has a method ``run()``

all of these are present if you extend :class:`GenericTheSydekickSimTestbench<acoretestbenches.generic_sim_testbenches.GenericTheSydekickSimTestbench>` or :class:`GenericTestbench<acoretestbenches.generic_sim_testbenches.GenericTestbench>`. Now, you can call ``python3 generic_tests.py`` with its different options to run the test, as long as you have selected the correct sim config in your test config.